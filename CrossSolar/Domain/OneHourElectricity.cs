﻿using System;

namespace CrossSolar.Domain
{
    public class OneHourElectricity
    {
        public int Id { get; set; }

        public int PanelId { get; set; } //Bug Fix: PanelId cannot be string but int, otherwise FK to Panel table cannot be created

        public Panel Panel { get; set; } //Bug Fix: Entity to reference the Panel.

        public long KiloWatt { get; set; }

        public DateTime DateTime { get; set; }
    }
}
﻿using Microsoft.EntityFrameworkCore;

namespace CrossSolar.Domain
{
    public class CrossSolarDbContext : DbContext
    {
        public DbSet<Panel> Panels { get; set; }
        public DbSet<OneHourElectricity> OneHourElectricitys { get; set; }

        public CrossSolarDbContext()
        {
        }

        public CrossSolarDbContext(DbContextOptions<CrossSolarDbContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //RCRUUIZP: this connection string is used for the ef console command (it ignores the connection string in the appsettings.json)
            optionsBuilder.UseSqlServer("Server=(local)\\MSSQLSERVER2017;Database=CrossSolarDb;Integrated Security=true;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Bug Fix: Add unique contraint for Panel Serial
            modelBuilder.Entity<Panel>()
                .HasAlternateKey(panel => panel.Serial)
                .HasName("UIX_Panel_Serial");
        }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CrossSolar.Models
{
    public class OneHourElectricityModel
    {
        public int Id { get; set; }

        [Required] public long KiloWatt { get; set; } //Bug Fix: This must be required

        [Required] public DateTime DateTime { get; set; } //Bug Fix: This must be required
    }
}
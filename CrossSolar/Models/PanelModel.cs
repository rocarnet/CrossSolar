﻿using System.ComponentModel.DataAnnotations;

namespace CrossSolar.Models
{
    public class PanelModel
    {
        public int Id { get; set; }

        [Required]
        [Range(-90, 90)]
        [RegularExpression(@"^[+|-]?\d*((.|,)\d{6})$")] //Bug Fix: Provided regular expression does not allows especify sign (+ or -), nor numbers without leading digits
        public double Latitude { get; set; }

        [Required] //Bug Fix: Longitude must be required 
        [RegularExpression(@"^[+|-]?\d*((.|,)\d{6})$")] //Bug Fix: Longitude values must have 6 decimals also.
        [Range(-180, 180)]
        public double Longitude { get; set; }

        [RegularExpression(@"^([A-Z]|[a-z]){4}\d{4}([A-Z]|[a-z]){4}\d{4}$")] //Bug Fix: Adding format validation for the serial
        [Required]//Bug Fix: marking as valid
        public string Serial { get; set; }

        public string Brand { get; set; }
    }
}
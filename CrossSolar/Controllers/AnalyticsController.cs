﻿using CrossSolar.Domain;
using CrossSolar.Models;
using CrossSolar.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CrossSolar.Controllers
{
    [Route("panel")]
    public class AnalyticsController : Controller
    {
        private readonly IAnalyticsRepository _analyticsRepository;
        private readonly IPanelRepository _panelRepository;

        public AnalyticsController(IAnalyticsRepository analyticsRepository, IPanelRepository panelRepository)
        {
            _analyticsRepository = analyticsRepository;
            _panelRepository = panelRepository;
        }

        // GET panel/XXXX1111YYYY2222/analytics
        [HttpGet("{serial}/[controller]")]
        public async Task<IActionResult> Get([FromRoute] string serial) //Bug Fix: the value in the route is not the panelId but the serial
        {
            var panel = await _panelRepository.Query()
                .FirstOrDefaultAsync(x => x.Serial.Equals(serial, StringComparison.CurrentCultureIgnoreCase));

            if (panel == null) return NotFound();

            var analytics = await _analyticsRepository.Query()
                .Where(x => x.PanelId == panel.Id).ToListAsync(); //Bug Fix: uses the panel.Id to do the filter instead of serial.

            var result = new OneHourElectricityListModel
            {
                OneHourElectricitys = analytics.Select(c => new OneHourElectricityModel
                {
                    Id = c.Id,
                    KiloWatt = c.KiloWatt,
                    DateTime = c.DateTime
                })
            };

            return Ok(result);
        }

        // GET panel/XXXX1111YYYY2222/analytics/day
        [HttpGet("{serial}/[controller]/day")]
        public async Task<IActionResult> DayResults([FromRoute] string serial)
        {
            //var result = new List<OneDayElectricityModel>();

            var panel = await _panelRepository.Query()
              .FirstOrDefaultAsync(pnl => pnl.Serial.Equals(serial, StringComparison.CurrentCultureIgnoreCase));

            if (panel == null) return NotFound();

            //This is not the best way to do it, even if we want to compute it on the fly.
            //Code First approach limitations makes more difficult to solve this use case. DB View or Stored procedure will provide much better perfomance
            
            var result = _analyticsRepository.Query()
                .Where(oneHourElectricity => oneHourElectricity.PanelId == panel.Id)
                .GroupBy(x => x.DateTime.Date)
                .Select(g => new OneDayElectricityModel
                {
                    DateTime = g.Key,
                    Sum = g.Sum(x=> x.KiloWatt),
                    Maximum = g.Max(x => x.KiloWatt),
                    Minimum = g.Min(x => x.KiloWatt),
                    Average = g.Average(x => x.KiloWatt)
                });

            return Ok(result);
        }

        // POST panel/XXXX1111YYYY2222/analytics
        [HttpPost("{serial}/[controller]")]
        public async Task<IActionResult> Post([FromRoute] string serial, [FromBody] OneHourElectricityModel value) //Bug Fix: the parameter panelId was changed to serial
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            //Bug Fix: searches the panel by serial to do validations
            var panel = await _panelRepository.Query() 
                .FirstOrDefaultAsync(pnl => pnl.Serial.Equals(serial, StringComparison.CurrentCultureIgnoreCase));

            if (panel == null) return NotFound(); //Bug Fix: validate that serial correspond to an existing panel.

            var oneHourElectricityContent = new OneHourElectricity
            {
                PanelId = panel.Id,
                KiloWatt = value.KiloWatt,
                DateTime = DateTime.UtcNow
            };

            await _analyticsRepository.InsertAsync(oneHourElectricityContent);

            var result = new OneHourElectricityModel
            {
                Id = oneHourElectricityContent.Id,
                KiloWatt = oneHourElectricityContent.KiloWatt,
                DateTime = oneHourElectricityContent.DateTime
            };

            return Created($"panel/{serial}/analytics/{result.Id}", result);
        }
    }
}
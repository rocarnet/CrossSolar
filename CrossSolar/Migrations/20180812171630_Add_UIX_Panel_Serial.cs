﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CrossSolar.Migrations
{
    public partial class Add_UIX_Panel_Serial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Serial",
                table: "Panels",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AddUniqueConstraint(
                name: "UIX_Panel_Serial",
                table: "Panels",
                column: "Serial");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "UIX_Panel_Serial",
                table: "Panels");

            migrationBuilder.AlterColumn<string>(
                name: "Serial",
                table: "Panels",
                nullable: false,
                oldClrType: typeof(string));
        }
    }
}

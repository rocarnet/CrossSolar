using System.Threading.Tasks;
using CrossSolar.Controllers;
using CrossSolar.Models;
using CrossSolar.Repository;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace CrossSolar.Tests.Controller
{
    public class PanelControllerTests
    {
        public PanelControllerTests()
        {
            _panelController = new PanelController(_panelRepositoryMock.Object);
        }

        private readonly PanelController _panelController;

        private readonly Mock<IPanelRepository> _panelRepositoryMock = new Mock<IPanelRepository>();

        [Fact]
        public async Task Register_ShouldInsertPanel()
        {
            var panel = new PanelModel
            {
                Brand = "Areva",
                Latitude = 12.345678,
                Longitude = 98.7655432,
                Serial = "AAAA1111BBBB2222"
            };

            // Arrange

            // Act
            var result = await _panelController.Register(panel);

            // Assert
            Assert.NotNull(result);

            var createdResult = result as CreatedResult;
            Assert.NotNull(createdResult);
            Assert.Equal(201, createdResult.StatusCode);
        }

        [Fact]
        public void Register_EmptySerial()
        {
            //Arrange 
            
            var panel = new PanelModel
            {
                Brand = "Areva",
                Latitude = 12.345678,
                Longitude = 98.755432,
                Serial = ""
            };
            var context = new ValidationContext(panel, null, null);
            var result = new List<ValidationResult>();

            //Act
            var valid = Validator.TryValidateObject(panel, context, result, true);

            //Assert
            Assert.False(valid);
        }
        [Fact]
        public void Register_NullSerial()
        {
            //Arrange 
            var panel = new PanelModel
            {
                Brand = "Areva",
                Latitude = 12.345678,
                Longitude = 98.765432,
                Serial = null
            };
            var context = new ValidationContext(panel, null, null);
            var result = new List<ValidationResult>();

            //Act
            var valid = Validator.TryValidateObject(panel, context, result, true);

            //Assert
            Assert.False(valid);
        }

        [Fact]
        public void Register_InvalidSmallLogintudSerial()
        {
            //Arrange 
            var panel = new PanelModel
            {
                Brand = "Areva",
                Latitude = 12.345678,
                Longitude = 98.765542,
                Serial = "AAA1111BBBB2222"
            };

            var context = new ValidationContext(panel, null, null);
            var result = new List<ValidationResult>();

            //Act
            var valid = Validator.TryValidateObject(panel, context, result, true);

            //Assert
            Assert.False(valid);
        }

        [Fact]
        public void Register_InvalidLongLongitudSerial()
        {
            //Arrange 
            var panel = new PanelModel
            {
                Brand = "Areva",
                Latitude = 12.345678,
                Longitude = 98.765542,
                Serial = "AAAA1111BBBB22222"
            };

            var context = new ValidationContext(panel, null, null);
            var result = new List<ValidationResult>();

            //Act
            var valid = Validator.TryValidateObject(panel, context, result, true);

            //Assert
            Assert.False(valid);
        }

        [Fact]
        public void Register_InvalidLongLongitud()
        {
            //Arrange 
            var panel = new PanelModel
            {
                Brand = "Areva",
                Latitude = 12.345678,
                Longitude = 98.72554321,
                Serial = "AAAA1111BBBB2222"
            };

            var context = new ValidationContext(panel, null, null);
            var result = new List<ValidationResult>();

            //Act
            var valid = Validator.TryValidateObject(panel, context, result, true);

            //Assert
            Assert.False(valid);
        }

        [Fact]
        public void Register_InvalidShortLongitud()
        {
            //Arrange 
            var panel = new PanelModel
            {
                Brand = "Areva",
                Latitude = 12.345678,
                Longitude = 98.65543,
                Serial = "AAAA1111BBBB2222"
            };
            var context = new ValidationContext(panel, null, null);
            var result = new List<ValidationResult>();

            //Act
            var valid = Validator.TryValidateObject(panel, context, result, true);

            //Assert
            Assert.False(valid);
        }

        [Fact]
        public void Register_InvalidShortLatitude()
        {
            //Arrange 
            var panel = new PanelModel
            {
                Brand = "Areva",
                Latitude = 12.34568,
                Longitude = 98.655434,
                Serial = "AAAA1111BBBB2222"
            };
            var context = new ValidationContext(panel, null, null);
            var result = new List<ValidationResult>();

            //Act
            var valid = Validator.TryValidateObject(panel, context, result, true);

            //Assert
            Assert.False(valid);
        }

        [Fact]
        public void Register_InvalidLongLatitude()
        {
            //Arrange 
            var panel = new PanelModel
            {
                Brand = "Areva",
                Latitude = 12.3456738,
                Longitude = 98.655432,
                Serial = "AAAA1111BBBB2222"
            };

            var context = new ValidationContext(panel, null, null);
            var result = new List<ValidationResult>();

            //Act
            var valid = Validator.TryValidateObject(panel, context, result, true);

            //Assert
            Assert.False(valid);
        }
        
    }
}
﻿using CrossSolar.Controllers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using CrossSolar.Repository;
using System.Threading.Tasks;
using CrossSolar.Models;
using System;

namespace CrossSolar.Tests.Controller
{
    public class AnalyticsControllerTests
    {
        private readonly AnalyticsController _analyticsController;
        private readonly Mock<IAnalyticsRepository> _analyticslRepositoryMock = new Mock<IAnalyticsRepository>();
        private readonly Mock<IPanelRepository> _panelRepositoryMock = new Mock<IPanelRepository>();


        public AnalyticsControllerTests()
        {
            _analyticsController = new AnalyticsController(_analyticslRepositoryMock.Object, _panelRepositoryMock.Object);
        }

        //[Fact]
        //public async Task Post_EmptyPanelSerial()
        //{
        //    var model = new OneHourElectricityModel
        //    {
        //        DateTime = DateTime.UtcNow,
        //        Id = 0,
        //        KiloWatt = 565432,
        //    };

        //    // Arrange

        //    // Act
        //    var result = await _analyticsController.Post("254852", model);

        //    // Assert
        //    Assert.Null(result);

        //    //var createdResult = result as CreatedResult;
        //    //Assert.NotNull(createdResult);
        //    //Assert.Equal(201, createdResult.StatusCode);
        //}
    }
}
